Java coding interview
=====================

https://coderpad.io/GCXMTCT4

```java
import java.io.*;
import java.util.*;

// There are 2 legs: left and right (left and right foot)
class Solution {
  public static void main(String[] args) {
    new Thread(new Foot("left")).start();
    new Thread(new Foot("right")).start();
  }
}

// We have "step" operation in loop: one step.
// We want to synchronize them so that it is strict order:
// Step left - Step right - Step left... etc.
class Foot implements Runnable {
  
  private final String name;
  
  public Foot(String name) {
    this.name = name;
  }

  @Override
  public void run() {
    for (int i = 0; i < 50; i++) { 
      step();
    }
  }

  private void step() {
    System.out.println("Step " + name);
  }
}
```

What potential problems do you see in this code?
```java

// What potential problems do you see in this code?
public class Class1 {
    public static String handle(Formatter f, String s) {
        if (s.isEmpty()) {
            return "empty";
        }
        return f.format(s.trim());
    }
}
```



public class Class1 {
    public static String handle(Formatter f, String s) {
        if (s == null) 
          return "null";
        if ( s.isEmpty()) {
            return "empty";
        }
        return f.format(s.trim());
    }
}



Consider the following Java code snippet, 
which is initializing two variables and both are not volatile, 
and two threads T1 and T2 are modifying these values as following, both are not synchronized
```java
int x = 0;
boolean bExit = false;

// Thread 1 (not synchronized)
x = 1; 
bExit = true;

// Thread 2 (not synchronized)
if (bExit) { 
   System.out.println("x=" + x);
}

// Is it possible for Thread 2 to print x=0?
```



Write a Java program to prevent deadlock in Java?
-------------------------------------------------
Some of the programming or coding interview question is always based on fundamental feature 
of Java programming language e.g. multi-threading, synchronization etc. 
Since writing deadlock proof code is important for a Java developer, 
programming questions which requires knowledge of concurrency constructs 
becomes popular coding question asked in Java Interviews.

The deadlock happens if four condition is true e.g. mutual exclusion, no waiting, circular wait and no preemption. If you can break any of this condition than you can create Java programs,which are deadlock proof.

One easy way to avoid deadlock is by imposing an ordering on acquisition and release of locks. You can further check How to fix deadlock in Java  to answer this Java programming questions with coding in Java




What are different types of classloaders?
-----------------------------------------
There are three types of built-in Class Loaders in Java:

Bootstrap Class Loader – It loads JDK internal classes, typically loads rt.jar and other core classes.
Extensions Class Loader – It loads classes from the JDK extensions directory, usually $JAVA_HOME/lib/ext directory.
System Class Loader – It loads classes from the current classpath that can be set while invoking a program using -cp or -classpath command line options.





