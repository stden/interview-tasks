package alm;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class TestEquals {
    @Test
    public void testStr() {
        String s1 = "a";
        String s2 = "a";
        String s3 = new String("a");
        assertTrue(s1 == s2);
        assertFalse(s1 == s3);
        assertTrue(s1.equals(s2));
        assertTrue(s1.equals(s3));
    }

    @Test
    public void testLng() {
        Long l1 = Long.valueOf(0x7F);
        Long l2 = Long.valueOf(0x7F);
        Long l3 = new Long(0x7F);
        assertTrue(l1 == l2);
        assertFalse(l1 == l3);
        assertTrue(l1.equals(l2));
        assertTrue(l1.equals(l3));
    }

    @Test
    public void testListSet() {
        List<Integer> l = new ArrayList<>();
        Set<Integer> s = new HashSet<>();
        l.add(1);
        l.add(1);
        l.add(2);
        s.add(1);
        s.add(1);
        s.add(2);
        assertEquals("[1, 1, 2]", l.toString());
        assertEquals("[1, 2]", s.toString());
    }

    /**
     * 1 2 3 can be shown in any order
     */
    @Test
    void testThreads() {
        Thread t1 = new Thread(() -> System.out.println("1"));
        Thread t2 = new Thread(() -> System.out.println("2"));
        t1.start();
        t2.start();
        System.out.println("3");
    }

}
