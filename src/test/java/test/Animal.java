package test;

interface Pl {
}

public class Animal<T extends Pl> {
    public T animal;

    public static void main(String[] args) {
        Animal<Dog> ad = new Animal<Dog>();
        ad.setAnimal(new Dog());
        Dog d = ad.getAnimal();
    }

    public T getAnimal() {
        return animal;
    }

    public void setAnimal(T animal) {
        this.animal = animal;
    }
}

class Dog implements Pl {
}

