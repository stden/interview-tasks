package test.X1;

public class Test {
    void f() throws MyException {
        throw new MyException();
    }

    public static void main(String[] args) throws MyException {
        MyException e1 = null;
        Test t = new Test();
        try {
            t.f();
        } catch (MyException e) {
            e1 = e;
            System.out.print("catch");
        } finally {
            System.out.print("finally");
            throw e1;
        }
        System.out.print("End");
    }
}

class MyException extends Exception {
}
