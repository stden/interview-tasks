package test;

public class A {
    public static void main(String[] s) {
        double d = 4.5;
        B b = new B();
        b.m1(d);
        System.out.println(d); // Результат 4.5
    }
}

class B {
    public double d;

    public void m1(double d) {
        d = d + 2.0; // Новое значение d никуда не сохраняется и не возрващается из функции
    }
}
