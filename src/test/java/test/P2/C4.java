package test.P2;

import test.P1.C1;
import test.P1.C2;
import test.P1.C3;

public class C4 {
    int i5;

    public static void main(String[] args) {
        // public-поля
        System.out.println(new C1().i1);
        System.out.println(new C2().i3);
        System.out.println(new C3().i4);
        System.out.println(new C4().i5);
        //System.out.println(new C4().i2);
    }
}
