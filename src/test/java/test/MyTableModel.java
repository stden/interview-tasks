package test;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

/**
 * Напишите код, создающий в окне формы элемент управления таблица, и отображающий в этой таблице массив знаяений
 * Массив также следует создать
 */
public class MyTableModel extends AbstractTableModel {
    final String[][] data;

    public MyTableModel(String[][] data) {
        this.data = data;
    }

    public static void main(String[] args) {
        // Создаём массив
        String[][] data = {
                {"1,1", "1,2", "1,3"},
                {"2,1", "2,2", "2,3"},
        };
        // Создаём окно
        JFrame frame = new JFrame("Таблица значений");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // Добавляем модель данных, её передаём в таблицу, таблицу добавляем на форму
        frame.add(new JTable(new MyTableModel(data)));
        frame.pack();
        frame.setVisible(true);
    }

    public int getRowCount() {
        return data.length;
    }

    public int getColumnCount() {
        return data[0].length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        return data[rowIndex][columnIndex];
    }
}
