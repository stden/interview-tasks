package test.threads;

public class A extends Thread {
    A(String threadName) {
        super(threadName);
    }

    public static void main(String[] args) {
        A a1 = new A("A1");
        A a2 = new A("A2");
        a2.start(); // Запускаем поток A2
        Thread.yield(); // текущий поток готов отказаться от процессора, но хотел бы, чтобы его запланировали как можно скорее
        // Планировщик может придерживаться или игнорировать эту информацию и имеет различное поведение в зависимости от ОС
        a1.run(); // Из главного "main" потока вызываем метод который выводит имя потока
        // В конце может быть выведено main A2 или A2 main
    }

    public void run() {
        m1(); // Выводим имя текущего потока
    }

    // Вывод имени текущего потока
    public synchronized void m1() {
        System.out.println(Thread.currentThread().getName());
    }
}
