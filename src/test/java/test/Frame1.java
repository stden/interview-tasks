package test;

import javax.swing.*;

// Исправьте код, добавляющий линейку меню в окно формы
public class Frame1 extends JFrame {
    public Frame1() {
        JMenuBar mb = new JMenuBar();

        mb.add(new JMenu("Menu item"));
        // setJMenuItem - такого метода нет
        // setMenu - такого тоже нет
        // action - бессмысленно тут вызывать
        // setJMenuBar - уже есть ниже
        // JMenu jMenu = new JMenu(); - тоже странно
        setJMenuBar(mb);
    }

    public static void main(String[] args) {
        Frame1 frame = new Frame1();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
