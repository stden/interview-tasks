package test;

import java.util.LinkedList;
import java.util.List;

public class Collect {
    public static void main(String[] args) {
        List<Number> l1 = new LinkedList<Number>(); // Можно
        //List<Number> l2 = new LinkedList<Integer>(); - невозможно привести X<Integer> к X<Number>
        //List<Number> l3 = (List<Number>) new LinkedList<Integer>(); - и так тоже нельзя
        //LinkedList<Integer> l4 = new LinkedList<int>(); - примитивные типы использовать нальзя в Generic'ах
        LinkedList<Integer> l5 = new <Integer>LinkedList(); // Можно, хотя так и не пишут
    }
}
