package regexp;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

class StringReplaceAllTest {
    @Test
    void testReplaceDoubleWords() {
        String s = "Goodbye bye bye world world world";
        assertEquals("Goodbye bye world", s.replaceAll("\\b(\\w+)\\b(\\s+(\\1))+", "$1"));
    }
}
