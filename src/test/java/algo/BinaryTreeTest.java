package algo;

import org.junit.Test;

import static algo.BinTreeNode.depth;
import static algo.BinTreeNode.isBalanced;
import static org.junit.Assert.*;

public class BinaryTreeTest {
    @Test
    public void testBinaryTree() {
        assertEquals(0, depth(null));
        assertTrue(isBalanced(null));
        BinTreeNode node = new BinTreeNode();
        assertEquals(1, depth(node));
        assertTrue(isBalanced(node));
        node.left = new BinTreeNode();
        assertTrue(isBalanced(node));

        node.left.left = new BinTreeNode();
        assertEquals(3, depth(node));
        assertFalse(isBalanced(node));
    }
}

