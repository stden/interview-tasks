import org.junit.jupiter.api.Test;

import java.util.InputMismatchException;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

public class ScannerTest {
    @Test
    public void testParseInts() {
        Scanner scan = new Scanner("1 2 test 3");
        assertEquals(1, scan.nextInt());
        assertEquals(2, scan.nextInt());
        try {
            scan.nextInt();
            fail("Должно быть исключение т.к. test - не число");
        } catch (InputMismatchException e) {
            assertNull(e.getMessage());
            assertEquals("test", scan.next());
        }
        assertEquals(3, scan.nextInt());
    }
}
