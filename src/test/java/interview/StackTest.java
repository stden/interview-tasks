package interview;

import org.junit.jupiter.api.Test;

import java.util.Stack;

public class StackTest {
    @Test
    public void testStack() {
        Stack<String> stack = new Stack<>();
        stack.push("1");
        stack.push("2");
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.isEmpty());
    }
}
