package interview.solutions;

/**
 * There are 2 legs: 2 instances (implements Runnable): left and right (left and right foot).
 * We have "step" operation in loop: one step.
 * We want to synchronize them so that it is strict order:
 * left-right-left-right
 */
public class FootSolution implements Runnable {

    private final String name;

    private FootSolution(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        new Thread(new FootSolution("left")).start();
        new Thread(new FootSolution("right")).start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 50; i++) {
            step();
        }
    }

    // Step left - Step right - Step left... etc.
    private void step() {
        try {
            synchronized (FootSolution.class) {
                System.out.println("Step " + name);
                FootSolution.class.notify();
                FootSolution.class.wait();
            }
        } catch (InterruptedException ignore) {
        }
    }
}
