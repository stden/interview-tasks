package interview.solutions;

/**
 * There are 2 legs: 2 instances (implements Runnable): left and right (left and right foot).
 * We have "step" operation in loop: one step.
 * We want to synchronize them so that it is strict order:
 * left-right-left-right
 */
public class Solution implements Runnable {
    private final String name;

    private Solution(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        new Thread(new Solution("left")).start(); // First thread
        new Thread(new Solution("right")).start(); // Second thread
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < 30; i++) {
                synchronized (Solution.class) {
                    step();

                    Solution.class.notify();
                    Solution.class.wait();
                }
            }
            synchronized (Solution.class) {
                Solution.class.notifyAll();
            }
        } catch (InterruptedException e) {
        }
        System.out.println("Finished: " + name);
    }

    // Step left - Step right - Step left... etc.
    private void step() {
        // ...
        System.out.println("Step " + name);
        // ...
    }
}

// 1. synchronized
// 2. wait / notify / notifyAll
