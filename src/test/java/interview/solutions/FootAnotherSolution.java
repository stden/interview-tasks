package interview.solutions;

/**
 * There are 2 legs: 2 instances (implements Runnable): left and right (left and right foot).
 * We have "step" operation in loop: one step.
 * We want to synchronize them so that it is strict order:
 * left-right-left-right
 */
class FootAnotherSolution implements Runnable {

    static final Object lock = new Object();
    private static String last = "left";
    private final String name;

    private FootAnotherSolution(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        new Thread(new FootAnotherSolution("left")).start(); // <--
        new Thread(new FootAnotherSolution("right")).start(); // <--
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) { // while(true) {
            step();
        }
        synchronized (FootAnotherSolution.class) {
            FootAnotherSolution.class.notifyAll();
        }
    }

    // Step left - Step right - Step left... etc.
    private void step() {
        // TODO:
        while (last.equals(name)) { // <--
            //System.out.println("name = " + name);
//            try {
//                Thread.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
        }
        //synchronized (lock) {
        System.out.println("Step " + name);

        last = name;
        // }

        synchronized (FootAnotherSolution.class) {
            try {
                FootAnotherSolution.class.notify();
                FootAnotherSolution.class.wait();
            } catch (InterruptedException ex) {
            }
        }

        //if(name.equals("left")){

        //    last = "left";
        //} else {
        //  last = "right";
        //}

        // ...

    }

    private class MyClass {
    }
}
