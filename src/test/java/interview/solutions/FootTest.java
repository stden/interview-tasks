package interview.solutions;


/**
 * Есть 2 ноги - 2 класса FootTest implements Runnable: left и right (левая и правая нога).
 * У них в бесконечном цикле выполняется операция step (один шаг), надо их синхронизировать чтобы было строгое
 * чередование: левой-правой-левой-правой
 */
class FootTest implements Runnable {

    private final String name;
    private int count;

    private FootTest(String name) {
        this.name = name;
        count = 0;
    }

    public static void main(String[] args) {
        new Thread(new FootTest("left")).start();
        new Thread(new FootTest("right")).start();
    }

    @Override
    public void run() {
        // В оригинале был бесконечный цикл while(true),
        // но я решил что лучше чтобы цикл кончался когда-нибудь :)
        for (int i = 0; i < 1000; i++) {
            try {
                step();
                synchronized (FootTest.class) {
                    FootTest.class.notify();
                    FootTest.class.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        synchronized (FootTest.class) {
            FootSolution.class.notifyAll();
        }
    }

    private void step() {
        System.out.println("Step " + name + " #" + (++count));
    }
}
