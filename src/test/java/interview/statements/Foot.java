package interview.statements;

/**
 * There are 2 test.threads: "legs"
 * 2 instances (implements Runnable): "left" and "right" foot.
 * We have "step" operation in loop: one step.
 * You have to synchronize them for strict order:
 * left-right-left-right
 */
public class Foot implements Runnable {
    private final String name;
    private static Integer SYNCHRONIZER = 1;
    private Foot(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        new Thread(new Foot("left")).start(); // First thread
        new Thread(new Foot("right")).start(); // Second thread
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            step();
        }
    }

    // Step left - Step right - Step left... etc.
    private void step() {
        // TODO: add synchronization
        System.out.println("Step " + name);
        // ...
        synchronized (this) {
            try {
                this.notifyAll();
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }
}

// 1. synchronized
//   - method
//   - static method
//   - block   synchronized(lock){ // other thread safe code } e.g. synchronized(this)
// 2. wait / notify / notifyAll
// Lock(), UnLock(), ReentrantLock(), TryLock()
