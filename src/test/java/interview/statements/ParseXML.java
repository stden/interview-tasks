package interview.statements;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Statement:
 * You are given a valid XML document, and you have to print its score.
 * The score is calculated by the sum of the score of each element.
 * For any element, the score is equal to the number of attributes it has.
 */
class ParseXML {
    @DisplayName("Test attributes method")
    @Test
    void testCountAttributesMulti() {
        assertEquals(1, attributes("<a href='https://google.com'>Google</a>"));
        assertEquals(1, attributes("<header text='='/>"));
        assertEquals(3, attributes("<point x='2.0' y='3.0' z='4.2'/>"));
        assertEquals(5, attributes("<feed xml:lang='en'>" +
                "    <title>XML 'test'</title>" +
                "    <subtitle lang='en'>Programming = =challenges</subtitle>" +
                "    <link rel='alternate' type='text/html' href='https://home.kuehne-nagel.com'/>" +
                "</feed>"));
    }

    @ParameterizedTest
    @CsvSource({"<a href='https://google.com'>Google</a>, 1", "<header text='='/>, 1",
            "<point x='2.0' y='3.0' z='4.2'/>, 3"})
    void testCountAttributes(String xml, int numberOfAttributes) {
        assertEquals(numberOfAttributes, attributes(xml));
    }

    int attributes(String xml) {
        // TODO: implement this method
        if(xml == null || xml.isEmpty()) {
            return 0;
        }

       String[] arr = xml.split("\\w+='[^']+']");
        return arr.length > 0 ? arr.length / 2 : 0;
    }
}

// Questions:
// * String class: charAt, substring, split
// * Regular Expressions, PCRE: pattern syntax?
// * XML-Parsers: Saxon - any experience?
// * DOM (Document Object Model)
// Testing:
// * What is the primary goal/benefit of Unit Testing for you?
// * What does TDD give us that we can’t get by adding tests after?
// * How to perform TDD?