package interview;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Проверка билета на счастливость :)
 */
class LuckyTicketsTest {
    @ParameterizedTest
    @ValueSource(strings = {"123213", "123321", "2204"})
    void positive(String number) {
        assertTrue(isMyTicketLucky(number));
    }

    @Test
    void negative() {
        assertFalse(isMyTicketLucky("123223"));
        assertFalse(isMyTicketLucky("34"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "1", "123"})
    void testExceptionForEmptyString(String number) {
        assertThrows(IllegalArgumentException.class, () -> {
            isMyTicketLucky(number);
        });
    }

    /**
     * Является ли билет счастливым? Длина номера билета должна быть чётной
     *
     * @param number номер билета
     * @return true - если является
     */
    boolean isMyTicketLucky(String number) {
        // Если строка пустая или длина нечётна
        if (number.isEmpty() || (number.length() % 2 != 0))
            throw new IllegalArgumentException("Номер не должен быть пустым и длина номера должна быть чётна " + number);
        int n = number.length() / 2; // Половина длины номера
        // Суммы цифр половинок номера
        int left = 0; // Сумма цифр левой половины
        int right = 0; // Сумма цифр правой половины
        for (int i = 0; i < n; i++) {
            left += number.charAt(i);
            right += number.charAt(i + n);
        }
        // Сравниваем половинки
        return left == right;
    }
}
