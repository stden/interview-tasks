package codility;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MinValue2 {

    int solution(int[] A, int[] B) {
        int n = A.length;
        int m = B.length;
        Arrays.sort(A);
        Arrays.sort(B);
        int i = 0;
        for (int value : A) {
            while (i < m - 1 && B[i] < value)
                i += 1;
            if (value == B[i])
                return value;
        }
        return -1;
    }

    @Test
    public void testSimple() {
        int[] A = new int[4];
        int[] B = new int[5];
        A[0] = 1;
        B[0] = 4;
        A[1] = 3;
        B[1] = 2;
        A[2] = 2;
        B[2] = 5;
        A[3] = 1;
        B[3] = 3;
        B[4] = 2;
        assertEquals(2, solution(A, B));
    }

    @Test
    public void test2() {
        int[] A = new int[2];
        int[] B = new int[2];
        A[0] = 2;
        A[1] = 1;
        B[0] = 3;
        B[1] = 3;
        assertEquals(-1, solution(A, B));
    }


    @Test
    public void test3() {
        assertEquals(-1, solution(new int[]{2}, new int[]{1}));
        assertEquals(1, solution(new int[]{1, 2},
                new int[]{-5, -4, -3, 1}));
    }
}
