import static org.junit.Assert.assertEquals;

/**
 * Statement:
 * ----------
 * You are given a valid XML document, and you have to print its score.
 * The score is calculated by the sum of the score of each element.
 * For any element, the score is equal to the number of attributes it has.
 * <p>
 * Sample Output: 5
 */
class Solution {
    public static void main(String[] args) {
        String xml = "<feed xml:lang='en'>\n" +
                "    <title>XML</title>\n" +
                "    <subtitle lang='en'>Programming = = =''challenges</subtitle>\n" +
                "    <link rel='alternate' type='text/html' href='https://home.kuehne-nagel.com'/>\n" +
                "    <updated>2013-12-25T12:00:00</updated>\n" +
                "</feed>";
        assertEquals(5, calculateAttributes(xml));
    }

    public static int calculateAttributes(String stringXml) {
        int attributes = 0;
        boolean inTag = false;
        for (int i = 0; i < stringXml.length(); i++) {
            if (stringXml.charAt(i) == '<') {
                inTag = true;
            }
            if (stringXml.charAt(i) == '>') {
                inTag = false;
            }
            if (inTag && stringXml.charAt(i) == '=') {
                attributes++;
            }
        }
        return attributes;
    }
}

// Test Driven Development: Red -> Green -> Refactoring - ?
// Code coverage - ?
// Continuous Integration: Jenkins - ?


//
//1. public class Class2 {
//2. public static String handle(Formatter f, String s){
//     if(s == null){
//      return "string is null"
//     }
//     if(f == null){
//       return "Formatter is null";
//     }
//3.   if(s.isEmpty()){
//4.     return "empty";
//5.   }
//      if(s.trim().isEmpty()){
//        return "no text";
//      }
//6.   return f.format(s.trim());
//7.   }
//8. }


