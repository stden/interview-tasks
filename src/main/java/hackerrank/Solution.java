package hackerrank;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {
    static class Cell {
        int row;
        int col;

        public Cell(int row, int col) {
            this.row = row;
            this.col = col;
        }

        public boolean equals(Object o) {
            if (this == o) return true;
            Cell cell = (Cell) o;
            return row == cell.row && col == cell.col;
        }

        public boolean inField(int n) {
            return row >= 0 && col >= 0 && (row <= n + 1) && (col <= n + 1);
        }

        public int hashCode() {
            return 31 * row + col;
        }

        public void move(int dr, int dc) {
            row += dr;
            col += dc;
        }
    }

    // Complete the queensAttack function below.
    static int queensAttack(int n, int k, int r_q, int c_q, int[][] obstacles) {
        char[][] field = new char[n + 2][n + 2];
        for (int i = 0; i <= n + 1; i++) {
            for (int j = 0; j <= n + 1; j++) {
                field[i][j] = '.';
            }
        }
        field[r_q][c_q] = 'Q';

        int sameRowL = 0;
        int sameRowR = n + 1;
        int sameColL = 0;
        int sameColR = n + 1;

        Cell mainDiagL = new Cell(r_q - n, c_q - n);
        while (!mainDiagL.inField(n)) mainDiagL.move(1, 1);

        Cell mainDiagR = new Cell(r_q + n, c_q + n);
        while (!mainDiagR.inField(n)) mainDiagR.move(-1, -1);

        Cell antiDiagL = new Cell(r_q - n, c_q + n);
        while (!antiDiagL.inField(n)) antiDiagL.move(1, -1);

        Cell antiDiagR = new Cell(r_q + n, c_q - n);
        while (!antiDiagR.inField(n)) antiDiagR.move(-1, +1);

        for (int[] obstacle : obstacles) {
            int row = obstacle[0];
            int col = obstacle[1];
            field[row][col] = 'X';
            if (row == r_q) { // Same row
                if (col < c_q && col > sameRowL) {
                    sameRowL = col;
                }
                if (col > c_q && col < sameRowR) {
                    sameRowR = col;
                }
            }
            if (col == c_q) { // Same col
                if (row < r_q && row > sameColL) {
                    sameColL = row;
                }
                if (row > r_q && col < sameRowR) {
                    sameColR = row;
                }
            }
            // Main diagonal
            if (row - r_q == col - c_q) {
                if (row < r_q && row > mainDiagL.row) {
                    mainDiagL.row = row;
                    mainDiagL.col = col;
                }
                if (row > r_q && row < mainDiagR.row) {
                    mainDiagR.row = row;
                    mainDiagR.col = col;
                }
            }
            // Anti diagonal
            if (row - r_q == c_q - col) {
                System.out.println("AntiDiag = " + row + " " + col);
                if (row < r_q && row > antiDiagL.row) {
                    antiDiagL.row = row;
                    antiDiagL.col = col;
                }
                if (row > r_q && row < antiDiagR.row) {
                    antiDiagR.row = row;
                    antiDiagR.col = col;
                }
            }
        }

        field[r_q][sameRowL] = '1';
        field[r_q][sameRowR] = '2';
        field[sameColL][c_q] = '3';
        field[sameColR][c_q] = '4';
        field[mainDiagL.row][mainDiagL.col] = '5';
        field[mainDiagR.row][mainDiagR.col] = '6';
        field[antiDiagL.row][antiDiagL.col] = '7';
        field[antiDiagR.row][antiDiagR.col] = '8';
        for (int i = n + 1; i >= 0; i--) {
            for (int j = 0; j <= n + 1; j++) {
                System.out.print(field[i][j]);
            }
            System.out.println();
        }

        // Calc cells
        int sameRow = (sameRowR - sameRowL - 2);
        int sameCol = (sameColR - sameColL - 2);
        int mainDiag = (mainDiagR.row - mainDiagL.row - 2);
        int antiDiag = (antiDiagR.row - antiDiagL.row - 2);
        System.out.println("sameRow = " + sameRow);
        System.out.println("sameCol = " + sameCol);
        System.out.println("mainDiag = " + mainDiag);
        System.out.println("antiDiag = " + antiDiag);
        return sameRow + sameCol + mainDiag + antiDiag;
    }

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(new FileReader("in.txt"));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("out.txt"));

        String[] nk = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nk[0]);

        int k = Integer.parseInt(nk[1]);

        String[] r_qC_q = scanner.nextLine().split(" ");

        int r_q = Integer.parseInt(r_qC_q[0]);

        int c_q = Integer.parseInt(r_qC_q[1]);

        int[][] obstacles = new int[k][2];

        for (int i = 0; i < k; i++) {
            String[] obstaclesRowItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int j = 0; j < 2; j++) {
                int obstaclesItem = Integer.parseInt(obstaclesRowItems[j]);
                obstacles[i][j] = obstaclesItem;
            }
        }

        int result = queensAttack(n, k, r_q, c_q, obstacles);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
