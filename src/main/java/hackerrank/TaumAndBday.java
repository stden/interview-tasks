import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class TaumAndBday {

    // Complete the taumBday function below.
    static int taumBday(long b, long w, long bc, long wc, long z) {
        long bw = b * bc + w * wc;
        long bb = (b + w) * bc + w * z;
        long ww = (b + w) * wc + b * z;
        System.out.println(bw + " " + bb + " " + ww);
        return (int) Math.min(bw, Math.min(bb, ww));
    }

    public static void main(String[] args) throws IOException {
        String ID = "TaumAndBday";
        Scanner scanner = new Scanner(new FileReader(ID + ".in"));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(ID + ".out"));

        int t = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int tItr = 0; tItr < t; tItr++) {
            String[] bw = scanner.nextLine().split(" ");

            int b = Integer.parseInt(bw[0]);

            int w = Integer.parseInt(bw[1]);

            String[] bcWcz = scanner.nextLine().split(" ");

            int bc = Integer.parseInt(bcWcz[0]);

            int wc = Integer.parseInt(bcWcz[1]);

            int z = Integer.parseInt(bcWcz[2]);

            int result = taumBday(b, w, bc, wc, z);

            bufferedWriter.write(String.valueOf(result));
            bufferedWriter.newLine();
        }

        bufferedWriter.close();

        scanner.close();
    }
}
