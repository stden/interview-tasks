package algo;

public class BinTreeNode {
    BinTreeNode left;
    BinTreeNode right;
    int depth;

    public static boolean isBalanced(BinTreeNode it) {
        if (it == null) return true;

        int leftDepth = (it.left == null) ? 0 : it.left.depth;
        int rightDepth = (it.right == null) ? 0 : it.right.depth;

        return Math.abs(leftDepth - rightDepth) <= 1
                && isBalanced(it.left) && isBalanced(it.right);
    }

    public static int depth(BinTreeNode it) {
        if (it == null) return 0;
        it.depth = Math.max(depth(it.left), depth(it.right)) + 1;
        return it.depth;
    }
}
