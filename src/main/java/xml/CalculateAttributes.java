package xml;

import static org.junit.Assert.assertEquals;

/**
 * Statement:
 * ----------
 * You are given a valid XML document, and you have to print its score.
 * The score is calculated by the sum of the score of each element.
 * For any element, the score is equal to the number of attributes it has.
 * <p>
 * Sample Output: 5
 */
class CalculateAttributes {
    public static void main(String[] args) {
        assertEquals(1, attributes("<a href='https://google.com'>Google</a>"));
        assertEquals(1, attributes("<header text='='/>"));
        assertEquals(3, attributes("<point x='2.0' y='3.0' z='4.2'/>"));
        assertEquals(5, attributes("<feed xml:lang='en'>" +
                "    <title>XML</title>" +
                "    <subtitle lang='en'>Programming = =challenges</subtitle>" +
                "    <link rel='alternate' type='text/html' href='https://home.kuehne-nagel.com'/>" +
                "    <updated>2013-12-25T12:00:00</updated>" +
                "</feed>"));
    }

    public static int attributes(String xml) {
        // TODO:
        //return 0;
        int attributes = 0;
        boolean inTag = false;
        boolean inString = false;
        for (int i = 0; i < xml.length(); i++) {
            final char c = xml.charAt(i);
            if (c == '<') {
                inTag = true;
            }
            if (c == '>') {
                inTag = false;
            }
            switch (c){
                case '\'':
                    inString = !inString;
                    break;
            }
            if (inTag && !inString && c == '=') {
                attributes++;
            }
        }
        return attributes;
    }
}


